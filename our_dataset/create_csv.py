# read the folder consisting of images and put them into csv file into column image_path

import os
import pandas as pd
from pathlib import Path

def create_csv():
    # get the current directory
    current_dir = os.getcwd()
    print(f'{current_dir=}')
    # get the path to the images
    path_to_images = Path(current_dir)
    print(f'{path_to_images=}')
    # get the list of images
    list_of_images = list(path_to_images.glob('*.jpg'))
    # remove path before the image name
    list_of_images = [str(image).split('/')[-1] for image in list_of_images]
    print(f'{list_of_images=}')
    # create a dataframe
    df = pd.DataFrame(list_of_images, columns=['image_path'])
    print(f'{df.head()}')
    # save the dataframe
    df.to_csv('our_dataset.csv', index=False)

if __name__ == '__main__':
    create_csv()