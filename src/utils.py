import torch
import torch.nn as nn
import torch.nn.functional as F

import time
import PIL
from PIL import Image
from glob import glob
import os
from pathlib import Path
import pandas as pd
import numpy as np
from torch.utils.data import DataLoader
import argparse
from tqdm import tqdm
import segmentation_models_pytorch as smp
from segmentation_models_pytorch.utils import losses
# from torchvision.models.segmentation import deeplabv3_resnet101
from torchvision.models.segmentation import fcn_resnet50, FCN_ResNet50_Weights
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
# from easydict import EasyDict as edict

from torchvision import models
# load kornia for the loading of the data (images)
import kornia
import cv2

from matplotlib import pyplot as plt
# apperently this is the new way to do it, one can pass multiple objects to the transform function
# https://pytorch.org/blog/extending-torchvisions-transforms-to-object-detection-segmentation-and-video-tasks/#the-new-transforms-api
import torchvision.transforms as transforms  
import albumentations as A

import mplcursors
import matplotlib as mpl


def get_args():
    """
    Function to read args
    """
    # create instance of parser
    parser = argparse.ArgumentParser()
    # args
    parser.add_argument('--train',  action='store_true')
    # name of the dataset to run the (train, test, etc.) on, default is rellis
    parser.add_argument('--dataset', type=str, default='rellis', choices=['kitti', 'rellis'])
    parser.add_argument('--test',  action='store_true')
    parser.add_argument('-b', '--batch_size', type=int, default=8)
    parser.add_argument('-e', '--epochs', type=int, default=10) # TODO change default (now set to 10 for testing purposes)
    parser.add_argument('--test_our', action='store_true')
    parser.add_argument('--view_kitty', action='store_true')
    parser.add_argument('--test_our_kitti', action='store_true')

    # parse the args
    args = parser.parse_args()

    return args


def run_desired_instance(args):
    """
    This function acts as a wrapper and will run instance of the program
    desired by the user
    The user has to specify the instance by providing args.
    """
    
    if args.train:
        train_model(args)
    elif args.test:
        test_model(args)
    elif args.test_our:
        test_our_dataset(args)
    elif args.view_kitty:
        view_kitty()
    elif args.test_our_kitti:
        test_our_dataset_kitti(args)
    else:
        print("No instance was specified, exiting...")


def load_image_PIL(image_path:str) -> PIL.Image:
    '''
    Will load an image from a given path
    :param image_path: path to the image
    :return: image of shape (HxWxC)
    '''
    image = Image.open(image_path) # load the image 

    return image

def simulate_args(): # simulate the args with the default values
    """
    This is essential for testing in kaggle notebooks
    """
    args = argparse.Namespace()
    args.train = True
    args.test = False
    args.batch_size = 8
    args.epochs = 10

    return args

def load_image_cv2(image_path:str):
    '''
    Will load an image from a given path
    :param image_path: path to the image
    :return: image of shape (HxWxC)
    '''
    image = cv2.imread(image_path) # load the image 
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # convert to RGB

    return image


def load_image_as_tensor(img: PIL.Image) -> torch.Tensor:
    '''
    Will load an image as a tensor
    :param img: image as PIL.Image of shape (HxWxC)
    :return: image as tensor (BxCxHxW)
    '''
    tensor = kornia.image_to_tensor(img, keepdim=True) # convert the image to a tensor
    # normalize to [0, 1]
    tensor = tensor.float() / 255.0
    
    return tensor




def get_desired_model(args, pretrained:bool=True) -> nn.Module: # TODO zistit v akom formate to ten deeplab chce
    '''
    Will load the HRNet model for the semantic segmentation task
    :return: model
    '''

    # model = torch.hub.load('HRNet/HRNet-Semantic-Segmentation:master', 'hrnet_w48_cityscapes', pretrained=False) #, force_reload=True)
    # model = torch.hub.load('WongKinYiu/yolov7', 'custom', pretrained=False,force_reload=True)
    # try deeplabv3_mobilenet_v3_large
    # model = torch.hub.load('pytorch/vision:v0.9.0', 'deeplabv3_resnet101', weights='DeepLabV3_ResNet101_Weights.DEFAULT') #, force_reload=True)
    # model = smp.Unet('mobilenet_v2', encoder_weights='imagenet', classes=35, activation=None, encoder_depth=5, decoder_channels=[256, 128, 64, 32, 16])
    # weights = FCN_ResNet50_Weights.DEFAULT

    model = models.segmentation.deeplabv3_resnet50(
            pretrained=pretrained, progress=True,
        ) # progress is for the download bar
    model.classifier = DeepLabHead(2048, 20) # 20 classes


    return model


def lst_to_csv(path_to_list: Path, path_to_save_df: Path) -> pd.DataFrame:
    '''
    Will convert a list to a pandas DataFrame
    :param path_to_list: path to the list
    :param path_to_save_df: path to save the DataFrame
    :return: success
    '''
    success = False
    dict_from_lst = read_lst(path_to_list)
    df = pd.DataFrame(dict_from_lst)
    if df is not None:
        df.to_csv(path_to_save_df, index=False)
        success = True

    if success:
        print('DataFrame saved successfully')
    else:
        print('DataFrame could not be saved')

    return None

def read_lst(path_to_list: Path) -> dict:
    
    # the lst file is a text file having 2 columns: image_path, mask_path
    # read it to the dictionary
    data = {'image_path': [], 'mask_path': []}
    with open(path_to_list, 'r') as f:
        for line in f:
            image_path, mask_path = line.split()
            data['image_path'].append(image_path)
            data['mask_path'].append(mask_path)

    return data



# class RellisDataset(torch.utils.data.Dataset):
#     def __init__(self, root:Path = Path('../'), dataset_folder:Path= Path('Rellis-3D'), split: str ='train', transform=None):
#         '''
#         Will load the Rellis dataset
#         :param root: path to the root directory
#         :param dataset_folder: folder containing the dataset images and masks
#         :param split: train, val, test -> the dataset dataframes must be in the root directory
#         :param transform: transform to apply to the images
#         '''

#         self.root = root
#         self.split = split
#         self.transform = transform
#         self.df = pd.read_csv(self.root / f'{split}.csv')
#         self.len_df = len(self.df)
#         self.full_image_path = self.root / dataset_folder
        
#         self.labels = {
#                     0: {"color": [0, 0, 0],  "name": "void"},
#                     1: {"color": [108, 64, 20],   "name": "dirt"},
#                     3: {"color": [0, 102, 0],   "name": "grass"},
#                     4: {"color": [0, 255, 0],  "name": "tree"},
#                     5: {"color": [0, 153, 153],  "name": "pole"},
#                     6: {"color": [0, 128, 255],  "name": "water"},
#                     7: {"color": [0, 0, 255],  "name": "sky"},
#                     8: {"color": [255, 255, 0],  "name": "vehicle"},
#                     9: {"color": [255, 0, 127],  "name": "object"},
#                     10: {"color": [64, 64, 64],  "name": "asphalt"},
#                     12: {"color": [255, 0, 0],  "name": "building"},
#                     15: {"color": [102, 0, 0],  "name": "log"},
#                     17: {"color": [204, 153, 255],  "name": "person"},
#                     18: {"color": [102, 0, 204],  "name": "fence"},
#                     19: {"color": [255, 153, 204],  "name": "bush"},
#                     23: {"color": [170, 170, 170],  "name": "concrete"},
#                     27: {"color": [41, 121, 255],  "name": "barrier"},
#                     31: {"color": [134, 255, 239],  "name": "puddle"},
#                     33: {"color": [99, 66, 34],  "name": "mud"},
#                     34: {"color": [110, 22, 138],  "name": "rubble"}
#                     }
        

#     # def _set_files(self):

#     #     self.images = sorted(glob(os.path.join(self.root, 'images', self.split, '*.jpg')))
#     #     self.masks = sorted(glob(os.path.join(self.root, 'annotations', self.split, '*.png')))

#     def __getitem__(self, idx):
        
#         im_path = os.path.join(self.full_image_path, self.df.iloc[idx]['image_path'])
#         mask_path = os.path.join(self.full_image_path, self.df.iloc[idx]['mask_path'])
#         image = load_image_cv2(im_path)
#         mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE) # load the mask as grayscale

#         if self.transform:
#             transformed = self.transform(image = image, mask = mask)   
#             image = Image.fromarray(transformed['image']) # convert to PIL
#             mask = transformed['mask']
        
#         if self.transform is None: 
#             image = Image.fromarray(image)

#         # just for the image
#         t = transforms.Compose([
#             transforms.ToTensor(),
#             transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
#         ])

#         image = t(image)
#         mask = torch.from_numpy(mask).long()
#         mask = self.convert_masks(mask)
#         mask = mask.squeeze(0) # remove the channel dimension as it is always 1
#         return image, mask

#     def __len__(self):
#         return self.len_df
    
#     def convert_masks(self, mask:torch.Tensor) -> torch.Tensor:
#         """
#         Masks are by default in the old format, this function will convert them to the new format
#         :param mask: mask to convert
#         :return: converted mask
#         """

#         old_label_format = {
#                         0: {"color": [0, 0, 0],  "name": "void"},
#                         1: {"color": [108, 64, 20],   "name": "dirt"},
#                         3: {"color": [0, 102, 0],   "name": "grass"},
#                         4: {"color": [0, 255, 0],  "name": "tree"},
#                         5: {"color": [0, 153, 153],  "name": "pole"},
#                         6: {"color": [0, 128, 255],  "name": "water"},
#                         7: {"color": [0, 0, 255],  "name": "sky"},
#                         8: {"color": [255, 255, 0],  "name": "vehicle"},
#                         9: {"color": [255, 0, 127],  "name": "object"},
#                         10: {"color": [64, 64, 64],  "name": "asphalt"},
#                         12: {"color": [255, 0, 0],  "name": "building"},
#                         15: {"color": [102, 0, 0],  "name": "log"},
#                         17: {"color": [204, 153, 255],  "name": "person"},
#                         18: {"color": [102, 0, 204],  "name": "fence"},
#                         19: {"color": [255, 153, 204],  "name": "bush"},
#                         23: {"color": [170, 170, 170],  "name": "concrete"},
#                         27: {"color": [41, 121, 255],  "name": "barrier"},
#                         31: {"color": [134, 255, 239],  "name": "puddle"},
#                         33: {"color": [99, 66, 34],  "name": "mud"},
#                         34: {"color": [110, 22, 138],  "name": "rubble"}
#         }

#         new_label_format = {
#             0: {"color": [0, 0, 0],  "name": "void"},
#             1: {"color": [108, 64, 20],   "name": "dirt"},
#             2: {"color": [0, 102, 0],   "name": "grass"},
#             3: {"color": [0, 255, 0],  "name": "tree"},
#             4: {"color": [0, 153, 153],  "name": "pole"},
#             5: {"color": [0, 128, 255],  "name": "water"},
#             6: {"color": [0, 0, 255],  "name": "sky"},
#             7: {"color": [255, 255, 0],  "name": "vehicle"},
#             8: {"color": [255, 0, 127],  "name": "object"},
#             9: {"color": [64, 64, 64],  "name": "asphalt"},
#             10: {"color": [255, 0, 0],  "name": "building"},
#             11: {"color": [102, 0, 0],  "name": "log"},
#             12: {"color": [204, 153, 255],  "name": "person"},
#             13: {"color": [102, 0, 204],  "name": "fence"},
#             14: {"color": [255, 153, 204],  "name": "bush"},
#             15: {"color": [170, 170, 170],  "name": "concrete"},
#             16: {"color": [41, 121, 255],  "name": "barrier"},
#             17: {"color": [134, 255, 239],  "name": "puddle"},
#             18: {"color": [99, 66, 34],  "name": "mud"},
#             19: {"color": [110, 22, 138],  "name": "rubble"}
#         }

#     # convert the masks to the new format
#         mask = mask.squeeze(0)
#         new_mask = torch.zeros_like(mask)
#         for old_label, new_label in zip(old_label_format.keys(), new_label_format.keys()):
#             new_mask[mask == old_label] = new_label

#         return new_mask.unsqueeze(0)


class RellisDataset(torch.utils.data.Dataset):
    def __init__(self, root:Path = Path('../'), dataset_folder:Path= Path('Rellis-3D'), split: str ='train', transform=None, is_mask:bool = True):
        '''
        Will load the Rellis dataset
        :param root: path to the root directory
        :param dataset_folder: folder containing the dataset images and masks
        :param split: train, val, test -> the dataset dataframes must be in the root directory
        :param transform: transform to apply to the images
        '''

        self.root = root
        self.split = split
        self.transform = transform
        self.df = pd.read_csv(self.root / f'{split}.csv')
        self.len_df = len(self.df)
        self.full_image_path = self.root / dataset_folder

        self.is_mask = is_mask
        
        self.labels = {
                    0: {"color": [0, 0, 0],  "name": "void"},
                    1: {"color": [108, 64, 20],   "name": "dirt"},
                    3: {"color": [0, 102, 0],   "name": "grass"},
                    4: {"color": [0, 255, 0],  "name": "tree"},
                    5: {"color": [0, 153, 153],  "name": "pole"},
                    6: {"color": [0, 128, 255],  "name": "water"},
                    7: {"color": [0, 0, 255],  "name": "sky"},
                    8: {"color": [255, 255, 0],  "name": "vehicle"},
                    9: {"color": [255, 0, 127],  "name": "object"},
                    10: {"color": [64, 64, 64],  "name": "asphalt"},
                    12: {"color": [255, 0, 0],  "name": "building"},
                    15: {"color": [102, 0, 0],  "name": "log"},
                    17: {"color": [204, 153, 255],  "name": "person"},
                    18: {"color": [102, 0, 204],  "name": "fence"},
                    19: {"color": [255, 153, 204],  "name": "bush"},
                    23: {"color": [170, 170, 170],  "name": "concrete"},
                    27: {"color": [41, 121, 255],  "name": "barrier"},
                    31: {"color": [134, 255, 239],  "name": "puddle"},
                    33: {"color": [99, 66, 34],  "name": "mud"},
                    34: {"color": [110, 22, 138],  "name": "rubble"}
                    }
        

    # def _set_files(self):

    #     self.images = sorted(glob(os.path.join(self.root, 'images', self.split, '*.jpg')))
    #     self.masks = sorted(glob(os.path.join(self.root, 'annotations', self.split, '*.png')))

    def __getitem__(self, idx):
        
        im_path = os.path.join(self.full_image_path, self.df.iloc[idx]['image_path'])
        if self.is_mask:
            mask_path = os.path.join(self.full_image_path, self.df.iloc[idx]['mask_path'])
        image = load_image_cv2(im_path)
        if self.is_mask:
            mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE) # load the mask as grayscale

        if self.transform:
            if self.is_mask:
                transformed = self.transform(image = image, mask = mask) 
            elif not self.is_mask:
                transformed = self.transform(image=image)  
            image = Image.fromarray(transformed['image']) # convert to PIL
            if self.is_mask:
                mask = transformed['mask']
        
        if self.transform is None: 
            image = Image.fromarray(image)

        # just for the image
        t = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            # transforms.Normalize( mean = [0.54219076, 0.64252184, 0.56622988], std=[0.19567323, 0.21091128, 0.28999415]) # make robust TODO:
        ])

        image = t(image)
        if self.is_mask:
            mask = torch.from_numpy(mask).long()
            mask = self.convert_masks(mask)
            mask = mask.squeeze(0) # remove the channel dimension as it is always 1
            return image, mask
        elif not self.is_mask:
            return image

    def __len__(self):
        return self.len_df
    
    def mean_std(self):
        '''
        Will calculate the mean and std of the dataset images
        '''
        # calc through 3 channels and calc original images, not the transformed ones
        mean = 0.
        std = 0.
        nb_samples = 0.
        for idx in tqdm(range(self.len_df)):
            im_path = os.path.join(self.full_image_path, self.df.iloc[idx]['image_path'])
            image = load_image_cv2(im_path)
            image = image / 255.0
            mean += image.mean(axis=(0,1))
            std += image.std(axis=(0,1))
            nb_samples += 1

        mean /= nb_samples
        std /= nb_samples

        return mean, std
    
    def convert_masks(self, mask:torch.Tensor, old_label_format = None, new_label_format=None) -> torch.Tensor:
        """
        Masks are by default in the old format, this function will convert them to the new format
        :param mask: mask to convert
        :return: converted mask

        default for rellist dataset
        """
        if old_label_format is None and new_label_format is None:

            old_label_format = {
                            0: {"color": [0, 0, 0],  "name": "void"},
                            1: {"color": [108, 64, 20],   "name": "dirt"},
                            3: {"color": [0, 102, 0],   "name": "grass"},
                            4: {"color": [0, 255, 0],  "name": "tree"},
                            5: {"color": [0, 153, 153],  "name": "pole"},
                            6: {"color": [0, 128, 255],  "name": "water"},
                            7: {"color": [0, 0, 255],  "name": "sky"},
                            8: {"color": [255, 255, 0],  "name": "vehicle"},
                            9: {"color": [255, 0, 127],  "name": "object"},
                            10: {"color": [64, 64, 64],  "name": "asphalt"},
                            12: {"color": [255, 0, 0],  "name": "building"},
                            15: {"color": [102, 0, 0],  "name": "log"},
                            17: {"color": [204, 153, 255],  "name": "person"},
                            18: {"color": [102, 0, 204],  "name": "fence"},
                            19: {"color": [255, 153, 204],  "name": "bush"},
                            23: {"color": [170, 170, 170],  "name": "concrete"},
                            27: {"color": [41, 121, 255],  "name": "barrier"},
                            31: {"color": [134, 255, 239],  "name": "puddle"},
                            33: {"color": [99, 66, 34],  "name": "mud"},
                            34: {"color": [110, 22, 138],  "name": "rubble"}
            }

            new_label_format = {
                0: {"color": [0, 0, 0],  "name": "void"},
                1: {"color": [108, 64, 20],   "name": "dirt"},
                2: {"color": [0, 102, 0],   "name": "grass"},
                3: {"color": [0, 255, 0],  "name": "tree"},
                4: {"color": [0, 153, 153],  "name": "pole"},
                5: {"color": [0, 128, 255],  "name": "water"},
                6: {"color": [0, 0, 255],  "name": "sky"},
                7: {"color": [255, 255, 0],  "name": "vehicle"},
                8: {"color": [255, 0, 127],  "name": "object"},
                9: {"color": [64, 64, 64],  "name": "asphalt"},
                10: {"color": [255, 0, 0],  "name": "building"},
                11: {"color": [102, 0, 0],  "name": "log"},
                12: {"color": [204, 153, 255],  "name": "person"},
                13: {"color": [102, 0, 204],  "name": "fence"},
                14: {"color": [255, 153, 204],  "name": "bush"},
                15: {"color": [170, 170, 170],  "name": "concrete"},
                16: {"color": [41, 121, 255],  "name": "barrier"},
                17: {"color": [134, 255, 239],  "name": "puddle"},
                18: {"color": [99, 66, 34],  "name": "mud"},
                19: {"color": [110, 22, 138],  "name": "rubble"}
            }

    # convert the masks to the new format
        mask = mask.squeeze(0)
        new_mask = torch.zeros_like(mask)
        for old_label, new_label in zip(old_label_format.keys(), new_label_format.keys()):
            new_mask[mask == old_label] = new_label

        return new_mask.unsqueeze(0)

    



# from the documentation: 
# Performance considerations
# We recommend the following guidelines to get the best performance out of the transforms:
#     Rely on the v2 transforms from torchvision.transforms.v2
#     Use tensors instead of PIL images
#     Use torch.uint8 dtype, especially for resizing
#     Resize with bilinear or bicubic mode

def get_augmentations_torch(): # this is deprecated and might be removed in the future
        '''
        Will apply the transformation to the image and mask
        :param image: image
        :param mask: mask
        :return: transformed image and mask
        '''

        # TODO: implement the transformation
        transform = transforms.Compose([
            # transforms.ToTensor(),  # from pil hxwxc in range [0, 255] to tensor c x h x w in range [0.0, 1.0]
            # transforms.ToTensor(),  # from pil hxwxc in range [0, 255] to tensor c x h x w in range [0.0, 1.0]
            transforms.ToImage(),  # from tensor c x h x w in range [0.0, 1.0] to pil hxwxc in range [0, 255]
            # transforms.ToDtype(torch.uint8, scale=True),  # optional, most input are already uint8 at this point
            # transforms.ToDtype(torch.uint8, scale=True),  # optional, most input are already uint8 at this point
            
            # ...
            # transforms.RandomResizedCrop(size=(224, 224), antialias=True),  # Or Resize(antialias=True)
            # ...
            # transforms.RandomHorizontalFlip(p=0.5),
            # float
            transforms.ToDtype(torch.float32, scale=True), # scale 
            # normalize only the image
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]), 

            # back to PIL hwc
            # transforms.ToPILImage(),


        ])


        return transform
    


def get_augmentations_albumentations(size:tuple =(600, 960), crop:tuple=(0, 960, 0, 600)) -> A.Compose: # (600, 960) is for the rellis dataset
    '''
    Will apply the transformation to the image and mask
    :param image: image
    :param mask: mask
    :return: transformed image and mask
    '''
    transform = A.Compose([
        A.Resize(size[0], size[1]),
        A.Crop(x_min=crop[0], x_max=crop[1], y_min=crop[2], y_max=crop[3]), # don't crop the image for rellis (the dims are og)
        # A.RandomCrop(500, 889),
        A.HorizontalFlip(p=0.5),
        A.VerticalFlip(p=0.5),
        # A.RandomRotate90(p=
        # 0.5),
        A.RandomBrightnessContrast(p=0.2),
        A.RandomGamma(p=0.2),
        A.OneOf([
            A.MotionBlur(blur_limit=5),
            A.MedianBlur(blur_limit=5),
            A.GaussianBlur(blur_limit=(3, 5)), # TODO: test this (3, 5) because i am not sure, before it was 5
            A.GaussNoise(var_limit=(5.0, 30.0)),
        ], p=0.7),

        A.OneOf([
            A.OpticalDistortion(distort_limit=1.0),
            A.GridDistortion(num_steps=5, distort_limit=1.),
            A.ElasticTransform(alpha=3),
            ], p=0.7),
        A.CLAHE(p=0.2),
        A.RGBShift(p=0.2),
        A.HueSaturationValue(p=0.2),
        A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=15, border_mode=0, p=0.85),
        #     A.Cutout(max_h_size=int(256 * 0.375), max_w_size=int(256 * 0.375), num_holes=1, p=0.7), #256 is image size
    ])

    return transform


def get_datasets(train:bool = False, val:bool=False, test:bool = False, dataset = 'rellis') -> tuple:
    '''
    Will get the datasets
    :param train: train dataset
    :param test: test dataset
    :param val: val dataset
    :return: train, test, val datasets
    '''

    train_ds, test_ds, val_ds = None, None, None
    if train:
        if dataset == 'rellis':
            train_ds = RellisDataset(split='train', transform=get_augmentations_albumentations())      
        elif dataset == 'kitti':
            train_ds = KittiDataset(root = Path('../'), 
                                    dataset_folder=Path('KITTI_dataset'), 
                                    split='train', 
                                    transform=get_augmentations_albumentations(
                                        size = (375, 1242), crop=(0, 1242, 10, 370)),
                                    is_mask=True, patches=True)
        else:
            raise ValueError(f'Dataset {dataset} not implemented')
    if test:
        if dataset == 'rellis':
            test_ds = RellisDataset(split='test', transform=get_valid_augmentations())
        elif dataset == 'kitti':
            test_ds = KittiDataset(root = Path('../'), 
                                    dataset_folder=Path('KITTI_dataset'), 
                                    split='train', 
                                    transform=get_valid_augmentations(size = (375, 1242), crop=(0, 1242, 10, 370)),
                                    is_mask=True, patches=True)
        else:
            raise ValueError(f'Dataset {dataset} not implemented')
    
    if val:
        if dataset == 'rellis':
            val_ds = RellisDataset(split='val', transform=get_valid_augmentations())
        elif dataset == 'kitti':
            val_ds = KittiDataset(root = Path('../'), 
                                    dataset_folder=Path('KITTI_dataset'), 
                                    split='train', 
                                    transform=get_valid_augmentations(size = (375, 1242), crop=(0, 1242, 10, 370)),
                                    is_mask=True, patches=True)
        else:
            raise ValueError(f'Dataset {dataset} not implemented')
        
    return train_ds, val_ds, test_ds


def train_model(args):
    print("Training the model")

    best_val_loss = float('inf')


    train_ds, val_ds, _ = get_datasets(train=True, val=True, dataset=args.dataset)
    train_loader, val_loader, _ = get_dataloaders(args, train_ds, val_ds)
    model = get_desired_model(args)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'{device=}')
    model.to(device)
    criterion = nn.CrossEntropyLoss()
    # criterion = losses.DiceLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)

    metrics_logger = metricsLogger(args)

    
    for epoch in tqdm(range(args.epochs)):    
        model.train()
        train_loss = 0
        train_pixel_acc = 0
        train_mIoU = 0
        with torch.set_grad_enabled(True):
            print('Training', end='\r')
            for i, (images, masks) in enumerate(tqdm(train_loader)):
                # get if patch or not from the dataset, the train_loder is running
                patches = train_loader.dataset.patches
                if patches:
                    batch_size, n_tiles, c, h, w = images.shape
                    images = images.view(-1, c, h, w) # flatten the batch and n_tiles
                    masks = masks.view(-1, h, w) # flatten the batch and n_tiles





                images = images.to(device)
                masks = masks.to(device)
                optimizer.zero_grad()
                outputs= model(images)['out']
                # outputs_test = F.softmax(outputs, dim=1)
                # outputs_test = torch.argmax(outputs_test, dim=1)
                

                # to float
                # print(f'{outputs_test.shape=}, {masks.shape=}')
                # print(f'{outputs_test=}')


                loss = criterion(outputs, masks)
                loss.backward()
                optimizer.step()

                # accuracy
                batch_pixel_acc = pixel_accuracy(outputs, masks)
                batch_mIoU = mIoU(outputs, masks)
                train_pixel_acc += batch_pixel_acc
                train_mIoU += batch_mIoU
                train_loss += loss.item()

        train_loss /= len(train_loader)
        train_pixel_acc /= len(train_loader)
        train_mIoU /= len(train_loader)        
        


        model.eval()
        valid_loss = 0
        valid_pixel_acc = 0
        valid_mIoU = 0
        with torch.no_grad():
            print('Validating', end='\r')
            for i, (images, masks) in enumerate(val_loader):
                # get if patch or not from the dataset, the train_loder is running
                patches = val_loader.dataset.patches
                if patches:
                    batch_size, n_tiles, c, h, w = images.shape
                    images = images.view(-1, c, h, w)
                    masks = masks.view(-1, h, w)

                images = images.to(device)
                masks = masks.to(device)
                outputs = model(images)['out']
                loss = criterion(outputs, masks)
                valid_pixel_acc += pixel_accuracy(outputs, masks)
                valid_mIoU += mIoU(outputs, masks)
                valid_loss += loss.item()

                

        valid_loss /= len(val_loader)
        valid_pixel_acc /= len(val_loader)
        valid_mIoU /= len(val_loader)


        metrics_logger.update(epoch, train_loss, train_pixel_acc, train_mIoU, valid_loss, valid_pixel_acc, valid_mIoU, optimizer.param_groups[0]['lr'])
        print(f'Epoch: {epoch}, Train Loss: {train_loss}, Train Pixel Acc: {train_pixel_acc}, Train mIoU: {train_mIoU}, Valid Loss: {valid_loss}, Valid Pixel Acc: {valid_pixel_acc}, Valid mIoU: {valid_mIoU}, LR: {optimizer.param_groups[0]["lr"]}')

        if valid_loss < best_val_loss:
            best_val_loss = valid_loss
            # torch.save(model.state_dict(), 'best_model.pth')
            torch.save(model, 'best_model.pth') # save whole modle instead of state_dict
            metrics_logger.save()



            



    return 

              

class metricsLogger():
    """ 
    This class is used to log the metrics during training and validation
    such as train loss, train accuracy, valid loss, valid accuracy, balanced val accuracy, etc
    """
    def __init__(self, args):
        self.args = args
        self.train_loss = []
        self.train_acc = []
        self.valid_loss = []
        self.valid_acc = [] 
        self.lr = []
        self.valid_balanced_acc = []
        self.epoch = []
        self.train_mIoU = []
        self.valid_mIoU = []

    def log_train(self, loss, acc, mIoU):
        """ 
        This function is used to log the train loss and train accuracy
        """
        self.train_loss.append(loss)
        self.train_acc.append(acc)
        self.train_mIoU.append(mIoU)

    def log_valid(self, loss, acc, mIoU):
        """
        This function is used to log the valid loss, valid accuracy and balanced valid accuracy
        """        
        self.valid_loss.append(loss)
        self.valid_acc.append(acc)
        self.valid_mIoU.append(mIoU)

    def log_lr(self, lr):
        """
        This function is used to log the learning rate
        """
        self.lr.append(lr)
    
    def log_epoch(self, epoch):
        """
        This function is used to log the epoch
        """
        self.epoch.append(epoch)

    def update(self, epoch, train_loss, train_acc, train_mIoU, valid_loss, valid_acc, valid_mIoU, lr):
        """
        This function is used to update all the metrics
        - epoch, train loss, train acc, valid loss, valid acc, valid balanced acc, lr
        """
        self.log_epoch(epoch)
        self.log_train(train_loss, train_acc, train_mIoU)
        self.log_valid(valid_loss, valid_acc, valid_mIoU)
        self.log_lr(lr)
    
    

    def save(self):
        """
        This function is used to save the metrics in a csv file
        the csv file will be saved in the current directory
        with the name in format <model_name>_metrics.csv
        """
        df = pd.DataFrame({ 'epoch': self.epoch,
                            'train_loss': self.train_loss,
                           'train_acc': self.train_acc,
                           'train_mIoU': self.train_mIoU,
                           'valid_loss': self.valid_loss,
                           'valid_acc': self.valid_acc,
                           'valid_mIoU': self.valid_mIoU,
                           'lr': self.lr
                           })
        df.to_csv(f'my_metrics.csv', index=False) # TODO: change this



def get_dataloaders(args:argparse.ArgumentParser, train_ds=None, val_ds=None, test_ds=None):
    """
    Function to get loaders from given datasets.
    :param train_ds:
    :param val_ds:
    :param test_ds:
    :return: train_dataloader, val_dataloader, test_dataloader
    """
    train_loader, val_loader, test_loader = None, None, None
    if train_ds is not None:
        train_loader = DataLoader(dataset = train_ds, batch_size = args.batch_size, shuffle = True)#,  num_workers=2)
    if val_ds is not None:
        val_loader = DataLoader(dataset = val_ds, batch_size = args.batch_size, shuffle = False)#,  num_workers=2)
    if test_ds is not None:
        test_loader = DataLoader(dataset = test_ds, batch_size = args.batch_size, shuffle = False)#,  num_workers=2)

    return train_loader, val_loader, test_loader 


# def get_train_transform():

#     return A.Compose([
#         A.Transpose(p=0.5),
#     A.VerticalFlip(p=0.5),
#     A.HorizontalFlip(p=0.5),
#     A.RandomBrightness(limit=0.2, p=0.75),
#     A.RandomContrast(limit=0.2, p=0.75),
#     A.OneOf([
#         A.MotionBlur(blur_limit=5),
#         A.MedianBlur(blur_limit=5),
#         A.GaussianBlur(blur_limit=5),
#         A.GaussNoise(var_limit=(5.0, 30.0)),
#     ], p=0.7),

#     A.OneOf([
#         A.OpticalDistortion(distort_limit=1.0),
#         A.GridDistortion(num_steps=5, distort_limit=1.),
#         A.ElasticTransform(alpha=3),
#     ], p=0.7),

#     A.CLAHE(clip_limit=4.0, p=0.7),
#     A.HueSaturationValue(hue_shift_limit=10, sat_shift_limit=20, val_shift_limit=10, p=0.5),
#     A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=15, border_mode=0, p=0.85),
#     A.Resize(256, 256),
#     A.Cutout(max_h_size=int(256 * 0.375), max_w_size=int(256 * 0.375), num_holes=1, p=0.7), #256 is image size
    
#     A.Normalize()
#     ])


def pixel_accuracy(output, target): 
    """
    Function to calculate the pixel accuracy

    :param output: model output
    :param target: target
    :return: pixel accuracy
    """
    # first argmax the output so that we get the class with the highest probability
    output = torch.argmax(F.softmax(output, dim=1), dim=1) #softmax along the channel dimension because we have multiple classes
    correct = torch.eq(output, target).int()
    valid = target.ne(255).int() # ignore the void class, however not needed probably
    acc_sum = (valid * correct).sum().float()
    valid_sum = valid.sum().float()
    acc = acc_sum / valid_sum

    acc = acc.cpu().detach().numpy()
    return acc


def mIoU(output, target, num_classes=20):
    """
    Function to calculate the mean Intersection over Union
    :param output: model output
    :param target: target
    :param num_classes: number of classes
    :return: mIoU
    """
    ious = []
    output = torch.argmax(F.softmax(output, dim=1), dim=1)

    for cls in range(num_classes):
        pred = output == cls
        target = target == cls
        intersection = (pred & target).float().sum((1, 2))  # Will be zero if Truth=0 or Prediction=0
        union = (pred | target).float().sum((1, 2))         # Will be zzero if both are 0

        iou = (intersection + 1e-6) / (union + 1e-6)  # We smooth our devision to avoid 0/0
        ious.append(iou.mean().item())
    return np.mean(ious)


def denormalize(image, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
    """
    Function to denormalize the image
    :param image: image [C, H, W]
    :param mean: mean
    :param std: std
    :return: denormalized image
    """
    if not isinstance(image, np.ndarray):
        image = image.numpy().astype(dtype=np.float32)
    for i in range(3):
        image[i] = (image[i] * std[i]) + mean[i]
    return np.transpose(image, (1, 2, 0)) # [H, W, C]


def test_model(args):
    """
    Function to test the model
    """
    _, _, test_ds = get_datasets(test=True, dataset=args.dataset)
    _, _, test_loader = get_dataloaders(args, test_ds=test_ds)
    # model = get_desired_model(args, pretrained=False)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # now we try to load whole model, instead of the state_dict (might be changed in later versions)
    model = torch.load('best_model.pth', map_location=device) # TODO: make this more robust
    model.to(device)
    criterion = nn.CrossEntropyLoss()
    model.eval()
    test_loss = 0
    test_pixel_acc = 0
    test_mIoU = 0
    with torch.no_grad():
        print('Testing', end='\r')
        for i, (images, masks) in enumerate(tqdm(test_loader)):
            # patches
            patches = test_loader.dataset.patches
            if patches:
                batch_size, n_tiles, c, h, w = images.shape
                images = images.view(-1, c, h, w)
                masks = masks.view(-1, h, w)


            images = images.to(device)
            masks = masks.to(device)
            outputs = model(images)['out'] # outputs are just masks
            for j, output in enumerate(outputs):
                print(images[j])
                visualize_im_and_mask(images[j], output, merged=True)
                 
            #    plot the image and the mask 
            # for i, output in enumerate(outputs):
                # mask = output.cpu().detach()
                # mask = torch.argmax(F.softmax(mask, dim=0), dim=0)
                # im = images.cpu().detach()[i]
                # print(f'{im.shape=}')
                # input()
                # fig, ax = plt.subplots(1, 2)
                # im = im.permute(1,2,0)
                # ax[0].imshow(im) # it expects image to be in the form of (HxWxC)
                # # mask = mask.squeeze(0)
                # # mask = mask.permute(1,2,0)
                # print(f'{mask.shape=}')
                # input()
                
                # ax[1].imshow(mask)

                # plt.show()

            loss = criterion(outputs, masks)
            test_pixel_acc += pixel_accuracy(outputs, masks)
            test_mIoU += mIoU(outputs, masks)
            test_loss += loss.item()

    test_loss /= len(test_loader)
    test_pixel_acc /= len(test_loader)
    test_mIoU /= len(test_loader)


    print(f'Test Loss: {test_loss}, Test Pixel Acc: {test_pixel_acc}, Test mIoU: {test_mIoU}')
    return


def get_valid_augmentations(size:tuple=(600, 960), crop:tuple=(0, 960, 0, 600)) -> A.Compose:
    """
    Function to get the validation augmentations
    # these are just valid , thus just resize and normalize
    """
    return A.Compose([
        A.Resize(size[0], size[1]),
        A.Crop(x_min=crop[0], x_max=crop[1], y_min=crop[2], y_max=crop[3]),
        # A.Normalize()
    ])


def visualize_im_and_mask(image, mask, merged:bool=False):
        #   plot the image and the mask 
        mask = mask.cpu().detach()
        mask = torch.argmax(F.softmax(mask, dim=0), dim=0)
        labels = np.unique(mask)
        # print(f'{labels=}')
        # input()
        # mask = mask_from_labels_to_colors(mask, get_new_label_format())
        # print(f'{mask.shape=}')
        im = image.cpu().detach()

        # print(f'{im.shape=}')
        # input()
        # fig, ax = plt.subplots(1, 2)
        # im = im.permute(1,2,0) # [C, H, W] -> [H, W, C]
        im = denormalize(im)

        # ax[0].imshow(im) # it expects image to be in the form of (HxWxC)
        # mask = mask.squeeze(0)
        # mask = mask.permute(1,2,0)
        # ax[1].imshow(mask)



        # add labels when hovering over the mask

        # label_format= get_new_label_format()
        
        label_format= KittiDataset().get_new_kitti_label_format()
        

        # show names of the classes when hovering over the mask

        # create the labels for the mask
        labels_to_names = {k: v['name'] for k, v in label_format.items()}
        labels_to_colors = {k: v['color'] for k, v in label_format.items()}
        if not merged:
            fig, ax = plt.subplots(1, 2, figsize=(12, 6))
    

            # image from -1 1 to 0 1
            
            ax[0].imshow(im)

            # Display the mask with a color map and range
            mask = mask.numpy()
            colors = [labels_to_colors[label] for label in labels]
            colors = np.array(colors) / 255  # Normalize to [0, 1]
            cmap = mpl.colors.ListedColormap(colors)

            ax[1].imshow(mask, cmap=cmap)

            # Use mplcursors to show the label name when hovering over the mask
            cursor = mplcursors.cursor(ax[1], hover=True)

            @cursor.connect("add")
            def on_add(sel):
                x, y = sel.target
                label = mask[int(y), int(x)]
                sel.annotation.set_text(labels_to_names[label])

        elif merged:
            fig, ax = plt.subplots(1, 1, figsize=(12, 6))   
            # do not display ticks
            ax.set_xticks([])
            ax.set_yticks([])
            # no borders
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_visible(False)
            # bbox inches tight
                        
            plt.tight_layout()



            
                     
            ax.imshow(im)
            mask = mask.numpy()
            # opacity of the mask 0.5 so it will be transparent
            print(f'{labels=}')
            
            
            colors = [labels_to_colors[label] for label in labels]
            colors = np.array(colors) / 255
            cmap = mpl.colors.ListedColormap(colors)

            
            ax.imshow(mask, cmap=cmap, alpha=0.5)
            cursor = mplcursors.cursor(ax, hover=True)


            @cursor.connect("add")
            def on_add(sel):
                x, y = sel.target
                label = mask[int(y), int(x)]
                annotation = sel.annotation
                annotation.set_text(labels_to_names[label])
            #     annotation.get_bbox_patch().set(fc='white', alpha=0.7)
            #     annotations.append(annotation)



            # handler = CursorHandler(ax, mask, labels_to_names)
            # cursor.connect("add", handler.on_add)


        plt.show()
        # save plot as image without borders
        

# class CursorHandler:
#     def __init__(self, ax, mask, labels_to_names):
#         self.ax = ax
#         self.mask = mask
#         self.labels_to_names = labels_to_names
#         self.annotations = []

#     def on_add(self, sel):
#         x, y = sel.target
#         label = self.mask[int(y), int(x)]
#         annotation = self.ax.annotate(
#             self.labels_to_names[label],
#             (x, y),
#             textcoords="offset points", xytext=(10,10),
#             ha='right', va='bottom',
#             bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
#             arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0')
#         )
        
#         self.annotations.append(annotation)
#         # plt.draw()
class CursorHandler:
    def __init__(self, ax, mask, labels_to_names):
        self.ax = ax
        self.mask = mask
        self.labels_to_names = labels_to_names
        self.annotations = []
    def on_add(self, sel):
        x, y = sel.target
        label = self.mask[int(y), int(x)]
        annotation = self.ax.annotate(
                self.labels_to_names[label],
                (x, y),
                textcoords="offset points", xytext=(50,50),  # Increase the xytext values to make the annotation bigger
                ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=1', fc='yellow', alpha=0.5),
                arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'),
                fontsize = 25
                # make the box bigger
                
            )
        self.annotations.append(annotation)
        sel.annotation.set_visible(False) 

        # draw the annotations
        plt.draw()



def get_our_dataset_augmentations():
   # the images are 720x1280 so cut them from sides without change of aspect raitio
    return A.Compose([
        A.Resize(600, 1067),
        # cutout 53 pixels from the left and 53 pixels from the right
        A.Crop(x_min=53, x_max=1067-53, y_min=0, y_max=600),
    
        # A.Normalize()
    ])
def test_our_dataset(args):
    our_ds = RellisDataset(root = Path('../our_dataset'), split='our_dataset', transform=get_our_dataset_augmentations(), dataset_folder=Path(''), is_mask=False)

    our_loader = DataLoader(dataset = our_ds, batch_size = args.batch_size, shuffle = False)#,  num_workers=2)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = torch.load('best_model.pth', map_location=device)
    
    model.to(device)
    model.eval()
    with torch.no_grad():
        for i, images in enumerate(tqdm(our_loader)):
            images = images.to(device)
            outputs = model(images)['out']
            for j, output in enumerate(outputs):
                visualize_im_and_mask(images[j], output, merged=True)



def get_our_dataset_augmentations_kitti():
    return A.Compose([
        A.Resize(360, 640),
        # A.Normalize()
    ])



def test_our_dataset_kitti(args):
    # our_ds = RellisDataset(root = Path('../our_dataset'), split='our_dataset', transform=get_our_dataset_augmentations_kitti(), dataset_folder=Path(''), is_mask=False)
    our_ds = KittiDataset(root = Path('../'), dataset_folder=Path('our_dataset'), split='our_dataset', transform=get_our_dataset_augmentations_kitti(), is_mask=False, patches=False)
    our_loader = DataLoader(dataset = our_ds, batch_size = args.batch_size, shuffle = False)#,  num_workers=2)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = torch.load('../weights/2024_05_10/best_model.pth', map_location=device)
    
    model.to(device)
    model.eval()
    with torch.no_grad():
        for i, images in enumerate(tqdm(our_loader)):
            images = images.to(device)
            outputs = model(images)['out']
            for j, output in enumerate(outputs):
                visualize_im_and_mask(images[j], output, merged=True)




def get_new_label_format():
    new_label_format = {
            0: {"color": [0, 0, 0],  "name": "void"},
            1: {"color": [108, 64, 20],   "name": "dirt"},
            2: {"color": [0, 102, 0],   "name": "grass"},
            3: {"color": [0, 255, 0],  "name": "tree"},
            4: {"color": [0, 153, 153],  "name": "pole"},
            5: {"color": [0, 128, 255],  "name": "water"},
            6: {"color": [0, 0, 255],  "name": "sky"},
            7: {"color": [255, 255, 0],  "name": "vehicle"},
            8: {"color": [255, 0, 127],  "name": "object"},
            9: {"color": [64, 64, 64],  "name": "asphalt"},
            10: {"color": [255, 0, 0],  "name": "building"},
            11: {"color": [102, 0, 0],  "name": "log"},
            12: {"color": [204, 153, 255],  "name": "person"},
            13: {"color": [102, 0, 204],  "name": "fence"},
            14: {"color": [255, 153, 204],  "name": "bush"},
            15: {"color": [170, 170, 170],  "name": "concrete"},
            16: {"color": [41, 121, 255],  "name": "barrier"},
            17: {"color": [134, 255, 239],  "name": "puddle"},
            18: {"color": [99, 66, 34],  "name": "mud"},
            19: {"color": [110, 22, 138],  "name": "rubble"}
        }
    return new_label_format


def mask_from_labels_to_colors(mask, label_format):
    """
    Function to convert the mask from labels to colors
    :param mask: mask
    :param new_label_format: new label format
    :return: mask in colors
    """
    mask = mask.squeeze(0)
    mask = mask.cpu().detach().numpy()
    mask = mask.astype(np.uint8)
    mask_color = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.uint8)
    for label, color in label_format.items():
        mask_color[mask == label] = color['color']
    return mask_color



class KittiDataset(torch.utils.data.Dataset):
    def __init__(self, root:Path = Path('../'), dataset_folder:Path= Path('KITTI_dataset'), split: str ='train', transform=None, is_mask:bool = True, patches:bool = True):
        '''
        Will load the Rellis dataset
        :param root: path to the root directory
        :param dataset_folder: folder containing the dataset images and masks
        :param split: train, val, test -> the dataset dataframes must be in the root directory
        :param transform: transform to apply to the images
        '''

        self.root = root
        self.split = split
        self.transform = transform
        self.df = pd.read_csv(self.root / dataset_folder /f'{split}.csv')
        self.len_df = len(self.df)
        self.full_image_path = self.root / dataset_folder
        self.patches = patches
        self.is_mask = is_mask
        
        self.labels = {
                    0: {"color": [0, 0, 0],  "name": "void"},
                    1: {"color": [108, 64, 20],   "name": "dirt"},
                    3: {"color": [0, 102, 0],   "name": "grass"},
                    4: {"color": [0, 255, 0],  "name": "tree"},
                    5: {"color": [0, 153, 153],  "name": "pole"},
                    6: {"color": [0, 128, 255],  "name": "water"},
                    7: {"color": [0, 0, 255],  "name": "sky"},
                    8: {"color": [255, 255, 0],  "name": "vehicle"},
                    9: {"color": [255, 0, 127],  "name": "object"},
                    10: {"color": [64, 64, 64],  "name": "asphalt"},
                    12: {"color": [255, 0, 0],  "name": "building"},
                    15: {"color": [102, 0, 0],  "name": "log"},
                    17: {"color": [204, 153, 255],  "name": "person"},
                    18: {"color": [102, 0, 204],  "name": "fence"},
                    19: {"color": [255, 153, 204],  "name": "bush"},
                    23: {"color": [170, 170, 170],  "name": "concrete"},
                    27: {"color": [41, 121, 255],  "name": "barrier"},
                    31: {"color": [134, 255, 239],  "name": "puddle"},
                    33: {"color": [99, 66, 34],  "name": "mud"},
                    34: {"color": [110, 22, 138],  "name": "rubble"}
                    }
        


    def __getitem__(self, idx):
        
        # im_path = os.path.join(self.full_image_path, self.df.iloc[idx]['image_path']) #TODO: mozno zovseobecnit
        im_path = self.df.iloc[idx]['image_path']
        if self.split == 'our_dataset': # TODO: toto krajsie osetrit
            im_path = os.path.join(self.full_image_path, im_path)
        print(im_path)
        if self.is_mask:
            # mask_path = os.path.join(self.full_image_path, self.df.iloc[idx]['mask_path'])
            mask_path = self.df.iloc[idx]['mask_path']
        image = load_image_cv2(im_path)
        if self.is_mask:
            # mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE) # load the mask as grayscale
            mask = load_image_cv2(mask_path) # loading as normal image because pojebany kitty dataset ma masky ako normalne obrazky
            # gen only first dimension of mask
            mask = mask[:,:,0] # other dimensions are not needed (they containt nejake totalne skurvene sracky)

        if self.transform:
            if self.is_mask:
                transformed = self.transform(image = image, mask = mask) 
            elif not self.is_mask:
                transformed = self.transform(image=image)  
            image = Image.fromarray(transformed['image']) # convert to PIL
            if self.is_mask:
                mask = transformed['mask']
        
        if self.transform is None: 
            image = Image.fromarray(image)

        # just for the image
        t = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            # transforms.Normalize( mean = [0.54219076, 0.64252184, 0.56622988], std=[0.19567323, 0.21091128, 0.28999415]) # make robust TODO:
        ])



        image = t(image)
        if self.is_mask:
            mask = torch.from_numpy(mask).long()
            mask = self.convert_masks(mask, self.get_kitti_label_format(), self.get_mask_converter_dict())
            mask = mask.squeeze(0) # remove the channel dimension as it is always 1
            
        if self.patches:
            if self.is_mask:
                img, mask = self.tiles(image, mask)
                return img, mask
            else:
                img = self.tiles(image)
                return img
        elif not self.patches and not self.is_mask:
            return image
        else:
            return image, mask # TODO osetrit?????/

    def __len__(self):
        return self.len_df
    

    def tiles(self, img, mask=None, tile_size=(360, 640), stride = 640-38):
        '''
        Will split the image and mask into tiles
        :param img: image
        :param mask: mask
        :param tile_size: size of the tile
        :param stride: stride
        :return: list of tiles
        '''
        tiles = []
        h, w = img.shape[1:]
        th, tw = tile_size
        
        img_patches = img.unfold(1, th, stride).unfold(2, tw, stride)
        img_patches = img_patches.contiguous().view(3, -1, th, tw)
        img_patches = img_patches.permute(1, 0, 2, 3)
        if mask is not None:
            mask_patches = mask.unfold(0, th, stride).unfold(1, tw, stride)
            mask_patches = mask_patches.contiguous().view(-1, th, tw)
            return img_patches, mask_patches
        else:
            return img_patches
        


#    def tiles(self, img, mask):

#         img_patches = img.unfold(1, 512, 512).unfold(2, 768, 768) 
#         img_patches  = img_patches.contiguous().view(3,-1, 512, 768) 
#         img_patches = img_patches.permute(1,0,2,3)
        
#         mask_patches = mask.unfold(0, 512, 512).unfold(1, 768, 768)
#         mask_patches = mask_patches.contiguous().view(-1, 512, 768)
        
    
    
    def mean_std(self):
        '''
        Will calculate the mean and std of the dataset images
        '''
        # calc through 3 channels and calc original images, not the transformed ones
        mean = 0.
        std = 0.
        nb_samples = 0.
        for idx in tqdm(range(self.len_df)):
            im_path = os.path.join(self.full_image_path, self.df.iloc[idx]['image_path'])
            image = load_image_cv2(im_path)
            image = image / 255.0
            mean += image.mean(axis=(0,1))
            std += image.std(axis=(0,1))
            nb_samples += 1

        mean /= nb_samples
        std /= nb_samples

        return mean, std
    
    def convert_masks(self, mask:torch.Tensor, old_label_format, converter_dict):
        """
        Masks are by default in the old format, this function will convert them to the new format
        :param mask: mask to convert
        :param old_label_format: old label format
        :param new_label_format: covnerter dict
        :return: converted mask

        """
        

    # convert the masks to the new format
        mask = mask.squeeze(0)
        new_mask = torch.zeros_like(mask)
        for old_label, new_label in zip(old_label_format.keys(), converter_dict.values()):
            new_mask[mask == old_label] = new_label

        return new_mask.unsqueeze(0)




    def get_kitti_label_format(self):
        label_format = {
            0: {"color": [128, 64, 128], "name": "road"},
            1: {"color": [244, 35, 232], "name": "sidewalk"},
            2: {"color": [70, 70, 70], "name": "building"},
            3: {"color": [102, 102, 156], "name": "wall"},
            4: {"color": [190, 153, 153], "name": "fence"},
            5: {"color": [153, 153, 153], "name": "pole"},
            6: {"color": [250, 170, 30], "name": "traffic light"},
            7: {"color": [220, 220, 0], "name": "traffic sign"},
            8: {"color": [107, 142, 35], "name": "vegetation"},
            9: {"color": [152, 251, 152], "name": "terrain"},
            10: {"color": [70, 130, 180], "name": "sky"},
            11: {"color": [220, 20, 60], "name": "person"},
            12: {"color": [255, 0, 0], "name": "rider"},
            13: {"color": [0, 0, 142], "name": "car"},
            14: {"color": [0, 0, 70], "name": "truck"},
            15: {"color": [0, 60, 100], "name": "bus"},
            16: {"color": [0, 80, 100], "name": "train"},
            17: {"color": [0, 0, 230], "name": "motorcycle"},
            18: {"color": [119, 11, 32], "name": "bicycle"},
            255: {"color": [0, 0, 0], "name": "void"}
        }
        return label_format
    
    def get_mask_converter_dict(self):
        converter_dict = {
            0: 1, # road
            1: 1, # sidewalk
            2: 2, # building
            3: 3, # wall
            4: 4, # fence
            5: 5, # pole
            6: 5, # traffic light
            7: 5, # traffic sign
            8: 6, # vegetation
            9: 7, # terrain
            10: 8, # sky
            11: 9, # person
            12: 10, # rider
            13: 10, # car
            14: 10, # truck
            15: 10, # bus
            16: 10, # train
            17: 11, # motorcycle
            18: 11, # bicycle
            255: 0 # void
        }
     
        return converter_dict

    def get_new_kitti_label_format(self): 
        # merged some classes which are similar
        label_format = {
            1: {"color": [128, 64, 128], "name": "road, sidewalk"},
            2: {"color": [70, 70, 70], "name": "building"},
            3: {"color": [102, 102, 156], "name": "wall"},
            4: {"color": [190, 153, 153], "name": "fence"},
            5: {"color": [153, 153, 153], "name": "pole, traffic light, traffic sign"},
            6: {"color": [107, 142, 35], "name": "vegetation"},
            7: {"color": [152, 251, 152], "name": "terrain"},
            8: {"color": [70, 130, 180], "name": "sky"},
            9: {"color": [220, 20, 60], "name": "person"},
            10: {"color": [255, 0, 0], "name": "rider, car, truck, bus, train"},
            11: {"color": [0, 0, 230], "name": "motorcycle, bicycle"},
            0: {"color": [0, 0, 0], "name": "void"}
        }
        return label_format



def view_kitty():

    kitti_ds = KittiDataset(root = Path('../'), dataset_folder=Path('KITTI_dataset'), split='train', transform=get_kitti_dataset_augmentations(), is_mask=True, patches=True)
    for i, data in enumerate(kitti_ds):
        
        if kitti_ds.patches:
            len_patches = data[0].shape[0]

            input()
            for patch_index in range(len_patches):
                image, mask = data[0][patch_index], data[1][patch_index]
                print(f'{image.shape=}')
                print(f'{mask.shape=}')
                image = torch.tensor(image)
                mask = torch.tensor(mask)
                # denormalize the image
                


            visualize_im_and_mask_without_network(image, mask, merged=True)


def visualize_im_and_mask_without_network(im, mask, merged=True):
    '''
    Same as function visualize_im_and_mask but without the network, thus we do not need to detach the mask,
    nor trying to argmax it
    '''
    
    labels = np.unique(mask)
        # print(f'{labels=}')
        # input()
        # mask = mask_from_labels_to_colors(mask, get_new_label_format())
        # print(f'{mask.shape=}')

        # print(f'{im.shape=}')
        # input()
        # fig, ax = plt.subplots(1, 2)
    print(f'{im.shape=}')
    print(f'{mask.shape=}')
    input()
    im = denormalize(im)

        # ax[0].imshow(im) # it expects image to be in the form of (HxWxC)
        # mask = mask.squeeze(0)
        # mask = mask.permute(1,2,0)
        # ax[1].imshow(mask)



        # add labels when hovering over the mask

    label_format= KittiDataset().get_new_kitti_label_format()

        # show names of the classes when hovering over the mask

        # create the labels for the mask
    labels_to_names = {k: v['name'] for k, v in label_format.items()}
    labels_to_colors = {k: v['color'] for k, v in label_format.items()}
    if not merged:
        fig, ax = plt.subplots(1, 2, figsize=(12, 6))
            # image from -1 1 to 0 1
            
        ax[0].imshow(im)

            # Display the mask with a color map and range
        mask = mask.numpy()
        colors = [labels_to_colors[label] for label in labels]
        colors = np.array(colors) / 255  # Normalize to [0, 1]
        cmap = mpl.colors.ListedColormap(colors)

        ax[1].imshow(mask, cmap=cmap)

            # Use mplcursors to show the label name when hovering over the mask
        cursor = mplcursors.cursor(ax[1], hover=True)

        @cursor.connect("add")
        def on_add(sel):
            x, y = sel.target
            label = mask[int(y), int(x)]
            sel.annotation.set_text(labels_to_names[label])

    elif merged:
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))            
        ax.imshow(im)
        mask = mask.numpy()
        # opacity of the mask 0.5 so it will be transparent
        
        colors = [labels_to_colors[label] for label in labels]
        colors = np.array(colors) / 255
        cmap = mpl.colors.ListedColormap(colors)
        ax.imshow(mask, cmap=cmap, alpha=0.5)
        cursor = mplcursors.cursor(ax, hover=True)

        @cursor.connect("add")
        def on_add(sel):
            x, y = sel.target
            label = mask[int(y), int(x)]
            sel.annotation.set_text(labels_to_names[label])
     
    
       
    plt.show()


    


def visualize_im(im):
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
            # image from -1 1 to 0 1
            
    ax.imshow(im)
    plt.show()



def get_kitti_dataset_augmentations():
   # the images are 720x1280 so cut them from sides without change of aspect raitio
    return A.Compose([
        A.Resize(375, 1242),
        A.Crop(x_min=0, x_max=1242, y_min=10, y_max=370)
        # cutout 53 pixels from the left and 53 pixels from the right
        # A.Crop(x_min=53, x_max=1067-53, y_min=0, y_max=600),
    
        # A.Normalize()
    ])



