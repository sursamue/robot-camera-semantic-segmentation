from utils import load_image_cv2
import cv2
from pathlib import Path
import numpy as np
import sys
import pandas as pd
from matplotlib import pyplot as plt
import torch
import albumentations as A
from PIL import Image


def read_kitti():

    np.set_printoptions(threshold=sys.maxsize)
    test_path = '/home/imr/Documents/robot-vision-image-semantic-segmentation/kitti-step/panoptic_maps/train/0001/000001.png'
    im = load_image_cv2(test_path)
    im = np.array(im)
    #get R
    # print('R')
    # print(im[:,:,0])

    print('G')
    # print(im[:, :, 1])
    # input()
    # print('B')
    print(im[:, :, 0])

    print(f'{im.shape}')


    return


def get_num_of_images(path):
    """recursive function to get the number of images in a folder and its subfolders"""
    p = Path(path)
    count = 0
    for x in p.iterdir():
        if x.is_dir():
            count += get_num_of_images(x)
        else:
            count += 1
    return count

# function create a csv with 'image_path' and 'mask_path' columns
def create_csv(split, images_path, masks_path):
    images = []
    masks = []
    im_path = Path(images_path)
    mask_path = Path(masks_path)

    # iter over the subfolders
    for x in im_path.iterdir():
        if x.is_dir():
            for im in x.iterdir():
                images.append(str(im))
                mask = mask_path / x.name / im.name
                masks.append(str(mask))

    df = pd.DataFrame({'image_path': images, 'mask_path': masks})
    df.to_csv(f'../KITTI_dataset/{split}.csv', index=False)



def get_kitti_dataset_augmentations():
   # the images are 720x1280 so cut them from sides without change of aspect raitio
    return A.Compose([
        A.Resize(750, 2484),
        A.Crop(x_min=500, x_max=2484-500, y_min=50, y_max=700),
        # cutout 53 pixels from the left and 53 pixels from the right
        # A.Crop(x_min=53, x_max=1067-53, y_min=0, y_max=600),
    
        # A.Normalize()
    ])


# A.Resize(600, 960),


def visualize_im(im):
    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
            # image from -1 1 to 0 1
            
    ax.imshow(im)
    plt.show()


if __name__=='__main__':

    im = load_image_cv2('/home/imr/Documents/robot-vision-image-semantic-segmentation/KITTI_dataset/images/train/0000/000000.png')

    t = get_kitti_dataset_augmentations()
    im = t(image = im)['image']
    # im = im.numpy()
    visualize_im(im)