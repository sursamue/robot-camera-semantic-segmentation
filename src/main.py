import torch
import torch.nn as nn
import PIL
from PIL import Image
from glob import glob
import os
from pathlib import Path
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
# easydict
# from easydict import EasyDict as edict

# load kornia for the loading of the data (images)
import kornia 



# my utils
import utils
from utils import RellisDataset



def main():

    # parse the argumens
    args = utils.get_args()
   
    # simulate args

    # model = utils.get_desired_model()
    
    # get model parameters
    # model = utils.get_desired_model()
    # num_params = sum(p.numel() for p in model.parameters())
    # print(f'{num_params=}')
    # for layer in model.children():
    #     print(layer)


    # get the desired instance
    train_ds = RellisDataset(split='train', transform=utils.get_augmentations_albumentations())
    # train mean=array([0.54219076, 0.64252184, 0.56622988]), std=array([0.19567323, 0.21091128, 0.28999415])

    val_ds = RellisDataset(split='val')
    # mean, std = train_ds.mean_std()
    # print(f'Val {mean=}, {std=}')
    # for idx, data in enumerate(val_ds):
    #     print(f'{idx=}')
    #     im, mask= data
    #     # print(im)

    #     input()

    test_ds = RellisDataset(split='test')
    utils.run_desired_instance(args)

    # instance


    
    
    # for i in range(len(train_ds)):
    #     img, mask = train_ds[i]
        
    #     print(f'{np.array(img).shape=}')
    #     print(f'{mask.shape=}')
    #     input()
    #     # plot the image and the mask 
    #     fig, ax = plt.subplots(1, 2)
    #     img = img.permute(1,2,0)
    #     ax[0].imshow(img) # it expects image to be in the form of (HxWxC)
    #     mask = mask.squeeze(0)
    #     # mask = mask.permute(1,2,0)
        
    #     ax[1].imshow(mask)

    #     plt.show()
  


        
        # input()

    return None




if __name__ == '__main__':
    main()